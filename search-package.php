	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Seven Seas Travels</title>
		<!-- core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/animate.min.css" rel="stylesheet">
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.transitions.css" rel="stylesheet">
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">    
		<link href="css/package.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" type="image/x-icon" href="images/ico/favicon.ico">
	    <style type="text/css">
	    	html {
	    		position: relative;
	    		min-height: 100%;
	    	}
	    	body {
	    		/* Margin bottom by footer height */
	    		margin-bottom: 86px;
	    	}
	    </style>
	</head>

	<body>

		<!-- Navigation -->
		<?php
		include ("header.php");
		?>

		
			<section id="portfolio" style="margin-top: -80px">

				<div class="container">	
					<h3 class="section-title wow fadeInDown">Search Results</h3>
					<?php
						require_once 'get-category.php';
						require_once 'php-script/package.php';
						if(isset($_POST) && !empty($_POST)){
							$string = $_POST['text_search'];
							$pkg_list = search_package_list($string);

							$cnt=0;
							if(empty($pkg_list) && count($pkg_list)<1){
								echo '<p>No Results</p>';
							}
							else{
								echo '<div class="portfolio-items">';
								foreach ($pkg_list as $pkg) {
									echo '<div class="portfolio-item package_'.$cnt.'">
										<div class="package_con">
											<div class="package_img">
												<img class="img-responsive img-portfolio img-hover" src="images/package/' . $pkg['images'][0] .'" alt="">
											</div>

											<a href="show-package.php?package=' . $pkg['id'] .'" class="package_details">
												<h4>'.$pkg['name'].'</h4>
												<p>'. $pkg['days_count'] .' Days / '. ($pkg['days_count']-1) .' Nights</p><span class="book_now_wrap">
												<span>Read More<i class="sprite icon_book_arrow"></i></span>
											</span></a>
										</div>
									</div>';
								}
								echo '</div>';
							}
						}
					?>
				</div>
			</section>
	<!-- /.container -->

	<!-- Footer -->
	<?php include ("footer.php");	?>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/mousescroll.js"></script>
	<script src="js/smoothscroll.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/jquery.isotope.min.js"></script>
	<script src="js/jquery.inview.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/main.js"></script>

</body>

</html>
