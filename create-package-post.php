<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Status</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">	
	<link href="css/font-awesome.min.css" rel="stylesheet">    
	<link href="css/main.css" rel="stylesheet">
	<link href="css/package.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>
		<!-- Navigation -->
		<?php
		require_once 'header.php';
		?>
		<br>
		<!-- Page Content -->
		<div class="container">		
			
			<?php
			include ("php-script/mysql_config.php");
			require_once 'php-script/upload_file.php';
			require_once 'php-script/package.php';

			$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

			if (!$con) {
				echo "Failed to connect to MySQL: " ;
				die('Error: ' );
			}


			if(isset($_POST['package_id']))
			{
				delete_package($_POST['package_id'],1);		
			}

		//var_dump();


			$package_name = $_POST['txt_package_name'];
			if($_POST['txt_package_cost']== ''){
				$package_cost = $_POST['txt_package_cost'];
			}
			else{
				$package_cost=0;
			}
			$package_desc = $_POST['txt_package_description'];
			$package_destinations = $_POST['txt_destinations'];
			$lat_long = $_POST['txt_lat_long'];



			$txt_day = $_POST["txt_day"];
			$txt_short_desc = $_POST["txt_short_desc"];
			$txt_long_desc = $_POST["txt_long_desc"];	


			$query = "INSERT INTO package (name,
			destinations,
			lat_long,
			cost,
			description,
			image_name)
			VALUES ('$package_name',
			'$package_destinations',
			'$lat_long',
			'$package_cost',
			'$package_desc',
			' ');";

			if (!mysqli_query($con, $query)) {
			///die('Error: ' . mysqli_error($con));
				echo "Package created". "<br>"; 			
			}
		//$result=mysqli_query($con,$query);
			$package_id = mysqli_insert_id($con);

		//Insert Days
		//echo "Package Created". "<br>"; 
			foreach ($txt_day as $a => $b) {
			//echo "a=".$a." b=".$b;
				$query = "INSERT INTO days (package_id,
				day,
				short_desc,
				long_desc)
				VALUES ($package_id,
				'$txt_day[$a]',
				'$txt_short_desc[$a]',
				'$txt_long_desc[$a]');";

				$result=mysqli_query($con,$query);
			}
			echo "Days Added". "<br>"; 

		//Update Category
			if(isset($_POST['select_category']))
			{
			//var_dump( $_POST["select_category"]);
				add_package_in_category($package_id, $_POST['select_category']);
			}

		//upload files
			if(isset($_FILES)&& $_FILES["files"]["size"]!=0)
			{
				$file_array=upload_file($_FILES,$package_id);
			//echo "Package Created". "<br>"; 
				foreach ($file_array as $file) {
				//echo "a=".$a." b=".$b;
					$query = "INSERT INTO package_images (package_id,
					file_name)
					VALUES ($package_id,
					'$file');";

					$result=mysqli_query($con,$query);
				//var_dump($file);
				}
			//var_dump($file_array);
				echo "Image Uploaded". "<br>";			
			}
			
			if(isset($_POST['package_id'])){
				change_image_package_id($_POST['package_id'],$package_id);
			}
		// else
		// {
		// 	$file_name=$_POST['package_image'];
		// 	//echo "Existing Image Used". "<br>";			
		// }

			if(isset($_POST['package_id']))
			{
				echo "Package modified successfully"; 		
			}
			else{
				echo "Package created successfully";
			}
			mysqli_close($con);
			?>

		</div>
		<!-- /.container -->
		
		<?php include ("footer.php");	?>
		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
	</html>