<!DOCTYPE html>
<html lang="en">

<?php
require_once 'php-script/package.php';
require_once 'php-script/config.php';

$requested_pkg = $_GET['package'];
$pkg_details= get_package_details($requested_pkg);

$package_dest = $pkg_details['destinations'];
$destinations = explode(";", $package_dest);
			//var_dump($destinations);

?>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title><?php echo $pkg_details['name'] . ' | 7SeasTravels.in' ?></title>
	<meta name="description" content="<?php echo $pkg_details['name'] . ' | 7SeasTravels.in' ?>">
	<meta name="author" content="">
	<meta name="keywords"
	content="<?php echo $common_meta_data_desc?>"
	lang="en-US" />

	<!-- core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.transitions.css" rel="stylesheet">
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/package.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" type="image/x-icon" href="images/ico/favicon.ico">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta charset="utf-8">
		<style type="text/css">
			html {
				position: relative;
				min-height: 100%;
			}
			body {
				/* Margin bottom by footer height */
				margin-bottom: 86px;
			}
			#map-canvas {
				width: 100%;
				height: 480px;
				margin: 0px;
				padding: 0px
			}
		</style>
		
		

	</head>

	<body >

		<!-- Navigation -->
		<?php
		require_once 'header.php';
		?>

		<section id="package_detail">
			<!-- Page Content -->
			<div class="container">	
				<br>
				<?php
			//include ("php-script/package.php");
			//$requested_pkg = $_GET['package'];
			//$pkg_details= get_package_details($requested_pkg);
				$package_id = $pkg_details['id'];
				$package_name = $pkg_details['name'];
				$package_cost = $pkg_details['cost'];
				$package_dest = $pkg_details['destinations'];
				$package_lat_long = $pkg_details['lat_long'];
				//echo $package_lat_long;
				$package_images = $pkg_details['images'];

				$days_list = get_package_days($requested_pkg);
			/*echo "<br/>***************************";
			 var_dump($days_list);
			 echo "<br/>***************************";*/
			 ?>
			 <h2><?php echo $package_name; ?></h2>
			 <div class="row" style="padding-top: 10px">			 	
			 	<div class="col-sm-8 wow fadeInRight" >			 		
			 		<input type="hidden" name="package_name" value="<?php echo  $package_name?>" />			 		

			 		<header id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 280px">
			 			<div class="carousel-inner">
			 				<?php
			 				$cnt = 0;
			 				foreach ($package_images as $image) {
			 					if($cnt++ == 0){
			 						echo '<div class="item active">
			 						<img class="img-responsive img-hover " src="images/package/'.$image.'" width="100%" height= "100%" alt=""> 						
			 					</div>';
			 				}
			 				else{
			 					echo '<div class="item">
			 					<img class="img-responsive img-hover " src="images/package/'.$image.'" width="100%"  height= "100%"  alt=""> 						
			 				</div>';
			 			}
			 		}
			 		?>
			 	</div>
			 	<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="icon-prev"></span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="icon-next"></span>
				</a>
			 	<!-- <img class="img-responsive img-hover " src="images/package/<?php echo $package_image?>" width="100%" alt=""> -->
			 </header>

			</div>
			<div class="col-sm-4 wow fadeInRight img-responsive img-hover" >				 
				
					<?php 
					echo '<h4>Destinations</h4><div class="col-sm-6">';
					$destArr = explode(";", $package_dest);
					$latLongArr = explode(";", $package_lat_long);
					$cnt = 0;
					foreach ($destArr as $dest) {
						if( $cnt++%5==0 && $cnt!=1){
							echo '</div><div class="col-sm-6">';
						}
						echo '<i class="fa fa-map-marker" aria-hidden="true" style="color: #F7584C"></i>&nbsp'.$dest.'<br>';
					}
					echo '</div>';
					?>
			 		<!-- <?php
				 		if($package_cost>0){
				 			echo '<h3>
				 			Cost: <small>'.$package_cost .'</small>
				 			</h3>';
				 		}
				 		?>	 -->
				 		
				 	</div>
				 </div>

				 <br>

				 <div class="row">			 	
				 	<div class="col-sm-8 wow fadeInRight">			 		
				 		<div id="package-detail">
				 			<ul id="myTab" class="nav nav-tabs  nav-justified">
				 				<li class="active">
				 					<a href="#itinerary" data-toggle="tab">
				 						Itinerary
				 					</a>
				 				</li>
				 				<li><a href="#map" data-toggle="tab" onClick="refreshMap();">Map</a></li>
				 				<li><a href="#terms" data-toggle="tab">Inclusion/Exclusion</a></li>


				 			</ul>
				 			<div id="myTabContent" class="tab-content">
				 				<div class="tab-pane fade in active" id="itinerary">
				 					<?php
				 					foreach ($days_list as $val) {
//var_dump($val);
				 						echo '<div>
				 						<div class="media-body">
				 							<div class="media-heading">
				 								<h4> Day '. $val['day'].':  '. $val['short_desc']  .'</h4> 
				 								<p>' . $val['long_desc'] . '</p>
				 							</div>
				 						</div>
				 					</div><hr width=100%>';
				 				}
				 				?>
				 			</div>
				 			<div class="tab-pane fade" id="map"> 	
				 				<div id="map-canvas" ></div>		
				 			</div>
				 			<div class="tab-pane fade" id="terms"> 
				 				<ul class="fa-ul">
				 					<?php
				 					if(is_internation_package($package_id)){
				 						echo '<li><span class="text-primary"><h4>Inclusion</h4></span>
				 						<ul class="fa-ul">						
				 							<li><i class="fa-li fa fa-check"></i>Arrival and departure airport transfers on seat in coach basis</li>
				 							<li><i class="fa-li fa fa-check"></i>All accomodetions in hotels as per itinery</li>
				 							<li><i class="fa-li fa fa-check"></i>Daily breakfast</li>
				 							<li><i class="fa-li fa fa-check"></i>Return economy class airfare</li>
				 							<li><i class="fa-li fa fa-check"></i>Visa (if applicable)</li>
				 						</ul>
					 					</li>
					 					<li><span class="text-primary"><h4>Exclusion</h4></span>
					 						<ul class="fa-ul">
					 							<li><i class="fa-li fa fa-close"></i>Any expenses of personal nature</li>
					 							<li><i class="fa-li fa fa-close"></i>Tips and porterage other than mentioned in the inclusion</li>
					 							<li><i class="fa-li fa fa-close"></i>Surcharges as applicable</li>
					 							<li><i class="fa-li fa fa-close"></i>Meals and  Beverages</li>
					 						</ul>
					 					</li>';
				 					}
				 					else{
				 						echo '<li><span class="text-primary"><h4>Inclusion</h4></span>
				 						<ul class="fa-ul">						
				 							<li><i class="fa-li fa fa-check"></i>Accommodation AS PER THE ITINERARY</li>
											<li><i class="fa-li fa fa-check"></i>All Hotel Taxes</li>
											<li><i class="fa-li fa fa-check"></i>All transfers and sightseeing as per Itinerary from Garage to Garage</li>
				 						</ul>
					 					</li>
					 					<li><span class="text-primary"><h4>Exclusion</h4></span>
					 						<ul class="fa-ul">
					 							<li><i class="fa-li fa fa-check"></i>Air Fare/Train Charges</li>
												<li><i class="fa-li fa fa-check"></i>All entrances, Boat ride & Guide charges</li>
												<li><i class="fa-li fa fa-check"></i>All other meals not mentioned above</li>
												<li><i class="fa-li fa fa-check"></i>All other personal expenses</li>
												<li><i class="fa-li fa fa-check"></i>All Toll and Parking Charges</li>
					 						</ul>
					 					</li>';
				 					}
				 					?>
				 				</ul>	
				 			</div>

				 			</div>
				 		</div>
				 	</div>
				 	<div class="col-sm-4 wow fadeInRight">			 		
				 		<section class="contact">
				 			<div class="contact-form">
				 				<h3>Quick Enquiry</h3>

				 				<form id="main-contact-form" name="contact-form" action="php-script/mailer.php" method="post">
				 					<div class="form-group">
				 						<input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
				 					</div>
				 					<div class="form-group">
				 						<input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
				 					</div>
				 					<div class="form-group">
				 						<input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number" required>
				 					</div>
				 					<div class="form-group">
				 						<textarea id="message" name="message" class="form-control" rows="8" placeholder="Message" required></textarea>
				 					</div>
				 					<button type="submit" class="btn btn-primary">Submit</button>
				 				</form>
				 			</div>
				 		</div>
				 	</div>			 


				 </div>
				 <!-- /.container -->
				</section>

				<!-- Footer -->
				<?php include ("footer.php");	?>
				<script src="js/jquery.js"></script>
				<script src="js/bootstrap.min.js"></script>
				<script src="js/owl.carousel.min.js"></script>
				<script src="js/mousescroll.js"></script>
				<script src="js/smoothscroll.js"></script>
				<script src="js/jquery.prettyPhoto.js"></script>
				<script src="js/jquery.isotope.min.js"></script>
				<script src="js/jquery.inview.min.js"></script>
				<script src="js/wow.min.js"></script>
				<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>

				<script>
					function initialize() {
						var locations = [
						<?php
						$cnt=1;
						foreach ($latLongArr as $latLog) {
							echo "['',". $latLog.",".$cnt++." ],";
						}
						?>
						// ['DESCRIPTION', 41.926979, 12.517385, 3],
						// ['DESCRIPTION', 41.914873, 12.506486, 2],
						// ['DESCRIPTION', 61.918574, 12.507201, 1],
						];

						window.map = new google.maps.Map(document.getElementById('map-canvas'), {
							mapTypeId: google.maps.MapTypeId.ROADMAP
						});

						var infowindow = new google.maps.InfoWindow();

						var bounds = new google.maps.LatLngBounds();

						for (i = 0; i < locations.length; i++) {
							marker = new google.maps.Marker({
								position: new google.maps.LatLng(locations[i][1], locations[i][2]),
								map: map
							});

							bounds.extend(marker.position);

							google.maps.event.addListener(marker, 'click', (function (marker, i) {
								return function () {
									infowindow.setContent(locations[i][0]);
									infowindow.open(map, marker);
								}
							})(marker, i));
						}

						map.fitBounds(bounds);

						var listener = google.maps.event.addListener(map, "idle", function () {
							//map.setZoom(10);
							google.maps.event.removeListener(listener);
						});
					}

					// function loadScript() {
					// 	var script = document.createElement('script');
					// 	script.type = 'text/javascript';
					// 	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&' + 'callback=initialize';
					// 	document.body.appendChild(script);
					// }

					function refreshMap() {
						//loadScript();
						//google.maps.event.trigger(document.getElementById('map-canvas'), 'resize');
						//map.setZoom( map.getZoom() );
						//alert("sdsd");
					}
					$(document).ready(function () {
						 //initialize();
						});
					$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
					  	//refreshMap();
					  	initialize();
					  });
//window.onload = initialize;

</script>

<script src="js/owl.carousel.min.js"></script>		
<script src="js/main.js"></script>	



</body>

</html>
