<footer id="footer">
    <div class="container">
        <div class="container hidden-xs">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2016 Seven Seas Travels.
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/Seven-Seas-Travels-851272694948164" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/sevenseastrave7" target="_blank"><i class="fa fa-twitter"></i></a></li>
                       <!--  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
        <div class="container visible-xs">
            <div class="row">
                <div class="col-md-4 text-center">
                    &copy; 2016 Seven Seas Travels.
                </div>
                <div class="col-md-4 text-center">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/Seven-Seas-Travels-851272694948164" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/sevenseastrave7" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <!-- <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!--/#footer-->