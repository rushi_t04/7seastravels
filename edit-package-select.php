<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Edit Package</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.transitions.css" rel="stylesheet">
	<link href="css/prettyPhoto.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/package.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>

		<!-- Navigation -->
		<?php 
		require_once 'header.php';
		?>
	</br>
	<!-- Page Content -->
	<div class="container">		
		<?php
		require_once 'php-script/package.php';
		$pkg_list = get_package_list();

		$pkg_option = '<option value="0">' . "Select" . '</option>';
		foreach ($pkg_list as $val) {
			$pkg_option = $pkg_option . '<option value='.$val['id'].'>' . $val['name'] . '</option>';
		}

		?>

		<div class="col-md-4">
			<select id="select_from" name="select_from[]" required="required"
			class="form-control" >
			<?php echo $pkg_option; ?>
		</select>
	</div>
	<button id="myButton" type="submit" class="btn btn-primary">
					Submit
				</button>
	<script type="text/javascript">
		document.getElementById("myButton").onclick = function () {
			var e = document.getElementById("select_from");
			if(e.options[e.selectedIndex].value!=0)
				location.href = "edit-package.php?package="+e.options[e.selectedIndex].value;

		};
	</script>



</div>
<!-- /.container -->

<!-- Footer -->
<?php include ("footer.php");	?>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/mousescroll.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/main.js"></script>

</body>

</html>
