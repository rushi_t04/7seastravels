<style>
    div.wrapper{  
        float:left; /* important */  
        position:relative; /* important(so we can absolutely position the description div */  
    }  
    div.description{  
        position:absolute; /* absolute position (so we can position it where we want)*/  
        bottombottom:0px; /* position will be on bottom */  
        left:0px;  
        
        /* styling bellow */  
        background-color:black;  
        font-family: 'tahoma';  
        font-size:15px;  
        color:white;  
        opacity:0.6; /* transparency */  
        filter:alpha(opacity=60); /* IE transparency */  
    }  
    p.description_content{  
        padding:10px;  
        margin:0px;  
    } 
	
	.bg-container{background-color:#696969}
</style>
<link href="css/custom.css" rel="stylesheet">
<?php	

function echo_package($pkg_list,$colomns)
{
	if(empty($pkg_list)) {
	echo '<div class="container">';
	echo '<div class="row"> 
			<div class="col-lg-3">
					No packages available for this destination
					</div>
					</div>';
			
            echo '</div>';
			return;
	}
	
	//echo '<div class="container">';		
					$total_pkg=count($pkg_list);
					//echo $total_pkg;
					for($cnt=0;$cnt<$total_pkg;$cnt++)
					{
						$val=$pkg_list[$cnt];
						
						//var_dump($val['category_list']);
						//echo $cnt%4 .", ";
						if($cnt%$colomns==0)
							echo '<div class="row">';
							
							if($colomns==3)
								echo '<div class="col-lg-4 pkg_col">';
							else if($colomns==4)
								echo '<div class="col-lg-3 pkg_col">';
        echo '<div class="package_con">
            <div class="package_img">
                <img class="img-responsive img-portfolio img-hover" src="images/package/' . $val['image_name'] .'" alt="" >
            </div>
           
            <a href="show-package.php?package='.$val['id'].'" target="_blank" class="package_details">
				<h4>' . $val['name'] . '</h4>
                <p>'. $val['days_count'] .' Days / '. ($val['days_count']-1) .' Nights</p>';
				if($val['cost']>0)
                echo '<span class="bk_nw_price">
                    <span>From </span>₹<b>' . $val['cost'] . '</b>
                </span>';
				
                echo '<span class="book_now_wrap">
                    <span>Book Now<i class="sprite icon_book_arrow"></i></span>
                </span></a>
        </div>';
		 if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']==1)
					echo '&nbsp;&nbsp;<a href="edit-package.php?package='.$val['id'].'" class="btn btn-primary" target="_blank">Edit</a>';
		echo '</div>';
						if($cnt%$colomns==($colomns-1))
							echo'</div></br>';
					}
					$cnt--;
					if($cnt%$colomns!=($colomns-1))
						echo'</div></br>';
					
			
           // echo '</div>';
}
function get_category($requested_category,$colomns=3)	
{
	$pkg_list = get_package_list($requested_category);
	echo_package($pkg_list,$colomns);
	
}

function get_searched_packages($search,$colomns=3)	
{
	$pkg_list = search_package_list($search);
	echo_package($pkg_list,$colomns);	
}
?>


<script>
   $(function () { $("[data-toggle='tooltip']").tooltip(); });
</script>