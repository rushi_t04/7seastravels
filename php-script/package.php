<?php

function get_package_list($category=-1) {

	include ("mysql_config.php");
	//echo "category=".$category;
	//echo $_POST["txt_package_name"];
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query="";
	if($category==-1)
		$query = "SELECT * FROM package ORDER BY name";
	else
		$query = "select package.* from tour_category left join package 
					on package.id=tour_category.package_id 
					where tour_category.category_id=".$category." ORDER BY name";
	$result = mysqli_query($con, $query);
	
	$pkg_list = Array();
	while ($row = mysqli_fetch_array($result)) {
			$category_list=get_package_category($row['id']);
			$images=get_package_images($row['id']);
			$pkg =Array( "id"=>$row['id'],
						"name"=>$row['name'],
			"cost"=> $row['cost'],
			"destinations"=>$row['destinations'],
			"images"=> $images,
			"category_list" => $category_list,
			"days_count" =>	get_package_number_of_days($row['id']));
			array_push($pkg_list, $pkg);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	//echo "Package Created";
	mysqli_close($con);
	
	return $pkg_list;
}

function get_domestic_packages() {

	include ("mysql_config.php");
	//echo "category=".$category;
	//echo $_POST["txt_package_name"];
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "select * from package where id not in (select package_id from tour_category where category_id=1)";
	$result = mysqli_query($con, $query);
	
	$pkg_list = Array();
	while ($row = mysqli_fetch_array($result)) {
			$category_list=get_package_category($row['id']);
			$images=get_package_images($row['id']);
			$pkg =Array( "id"=>$row['id'],
						"name"=>$row['name'],
			"cost"=> $row['cost'],
			"destinations"=>$row['destinations'],
			"images"=> $images,
			"category_list" => $category_list,
			"days_count" =>	get_package_number_of_days($row['id']));
			array_push($pkg_list, $pkg);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	//echo "Package Created";
	mysqli_close($con);
	
	return $pkg_list;
}

function get_package_images($requested_pkg)
{
		include ("php-script/mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
		$images_list = Array();
			$query = "SELECT * FROM package_images where package_id=$requested_pkg";
			$result = mysqli_query($con, $query);
			while ($row = mysqli_fetch_array($result)) {

				//echo "hi"+$row['location_from'];
				$file_name = $row['file_name'];

				array_push($images_list, $file_name);
			}
			mysqli_close($con);
			return $images_list;
	
}

function get_package_number_of_days($requested_pkg)
{
		include ("php-script/mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
		$days_list = Array();
			$query = "SELECT * FROM days where package_id=$requested_pkg ORDER BY day";
			$result = mysqli_query($con, $query);
			mysqli_close($con);
			if( $result !== false ){
				return mysqli_num_rows($result);
			}
			else{		
				return 0;
			}
	
}

function get_all_destination_list()
{
	include ("mysql_config.php");
	//echo "category=".$category;
	//echo $_POST["txt_package_name"];
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT destinations FROM package ORDER BY name";
	$result = mysqli_query($con, $query);
	$destination_list = Array();
	while ($row = mysqli_fetch_array($result)) 
	{
		$package_dest = $row['destinations'];
		$destinations = explode(";", $package_dest);
		foreach ($destinations as $val)
		{
			$val=trim($val);
			array_push($destination_list, $val);
		}
	}
	$destination_list=array_unique($destination_list);
	var_dump($destination_list);
}

function search_package_list($search) {

include ("mysql_config.php");
	//echo "category=".$category;
	//echo $_POST["txt_package_name"];
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query="";
	$query = "SELECT * FROM package WHERE destinations like '%".$search."%' OR name like '%".$search."%'";
	
	$result = mysqli_query($con, $query);
	
	$pkg_list = Array();
	while ($row = mysqli_fetch_array($result)) {
		$category_list=get_package_category($row['id']);
		$images=get_package_images($row['id']);
			$pkg =Array( "id"=>$row['id'],
						"name"=>$row['name'],
			"cost"=> $row['cost'],
			"destinations"=>$row['destinations'],
			"images"=> $images,
			"category_list" => $category_list,
			"days_count" =>	get_package_number_of_days($row['id']));
				
			array_push($pkg_list, $pkg);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	//echo "Package Created";
	mysqli_close($con);
	
	return $pkg_list;
}

function get_location_list()
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT * FROM location_master";
	$result = mysqli_query($con, $query);
	
	$location_list = Array();
	while ($row = mysqli_fetch_array($result)) {
			$location =Array( "id"=>$row['id'],
						"name"=>$row['name']);
				
			array_push($location_list, $location);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	
	mysqli_close($con);
	
	return $location_list;
	
}

function get_travel_mode_list()
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT * FROM travel_mode_master";
	$result = mysqli_query($con, $query);
	
	$location_list = Array();
	while ($row = mysqli_fetch_array($result)) {
			$location =Array( "id"=>$row['id'],
						"name"=>$row['name']);
				
			array_push($location_list, $location);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	
	mysqli_close($con);
	
	return $location_list;
	
}

function get_package_details($requested_pkg)
{
	include ("php-script/mysql_config.php");

			//echo $_GET['package'];
			$pkg_details = Array();
			$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			$query = "SELECT * FROM package where id=$requested_pkg";
			$result = mysqli_query($con, $query);
			$row = mysqli_fetch_array($result);

			/*echo $row['id'];
			 echo $row['name'];
			 echo $row['cost'];
			 echo $row['description'];*/			
			mysqli_close($con);	

			$images=get_package_images($requested_pkg);

			return Array("id" => $row['id'], 
						"name" =>$row['name'],
						"cost" => $row['cost'],
						"destinations" =>$row['destinations'],
						"lat_long" =>$row['lat_long'],
						"description" =>$row['description'],
						"images" => $images);
	
}

function get_package_days($requested_pkg)
{
		include ("php-script/mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
		$days_list = Array();
			$query = "SELECT * FROM days where package_id=$requested_pkg ORDER BY day";
			$result = mysqli_query($con, $query);
			while ($row = mysqli_fetch_array($result)) {

				//echo "hi"+$row['location_from'];
				$day = $row['day'];
				$short_desc = $row['short_desc'];
				$long_desc = $row['long_desc'];

				$day = Array("day" => $day,"short_desc" => $short_desc, "long_desc" => $long_desc);

				array_push($days_list, $day);
			}
			mysqli_close($con);
			return $days_list;
	
}

function delete_package($package_id,$keep_image=0)
{
	include ("php-script/mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
	//echo $package_id;
	$query = "SELECT * FROM package WHERE id=".$package_id;
	$result = mysqli_query($con, $query);

	if($row = mysqli_fetch_array($result) && $keep_image==0)
	{
		// $image_name=$row['image_name'];	
		// //echo $image_name;
		// unlink('images/package/'.$image_name);
		echo "deleting images";
		delete_package_images($package_id);

	}

	$query = "DELETE FROM tour_category WHERE package_id='".$package_id."'";	
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting catagory". "</br>"; 
		
	}
	
	$query = "DELETE FROM days WHERE package_id='".$package_id."'";
	
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting days". "</br>"; 
		
	}
	$query = "DELETE FROM package WHERE id='".$package_id."'";

	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting pkg". "</br>"; 
		
	}
	mysqli_close($con);	
}

function delete_image($file,$package_id)
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	//echo $package_id;	
	$query = "DELETE FROM package_images WHERE package_id='".$package_id."' and file_name='".$file."'";	
	//echo $query;
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting file". "</br>"; 
		
	}	
	mysqli_close($con);	

	unlink('images/package/'.$file);
}

function delete_package_images($package_id){
	$images = get_package_images($package_id);

	if(isset($images) && !empty($images)){
		include ("mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
		if (mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
		//echo $package_id;	
		$query = "DELETE FROM package_images WHERE package_id='".$package_id."'";	
		//echo $query;
		
		if (!mysqli_query($con, $query)) {
			///die('Error: ' . mysqli_error($con));
			echo "error while deleting file". "</br>"; 
			
		}	
		mysqli_close($con);	
		foreach ($images as $image) {
			unlink('images/package/'.$image);
		}
	}
}

function change_image_package_id($old_package_id,$new_package_id)
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	//echo $package_id;	
	$query = "UPDATE package_images SET package_id='".$new_package_id."' WHERE package_id='".$old_package_id."'";	
	//echo $query;
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while updating file". "</br>"; 
		
	}	
	mysqli_close($con);	
}

function get_category_list()
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT * FROM category_master";
	$result = mysqli_query($con, $query);
	
	$category_list = Array();
	while ($row = mysqli_fetch_array($result)) {
			$category =Array( "id"=>$row['id'],
						"name"=>$row['name']);
				
			array_push($category_list, $category);
		}
	
	
	if (!mysqli_query($con, $query)) {
		die('Error: ' . mysqli_error($con));
	}
	
	mysqli_close($con);
	
	return $category_list;
}
function get_package_category($package)
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT * FROM tour_category where package_id=".$package;
	$result = mysqli_query($con, $query);
	
	$category_list = Array();

	if( $result !== false ){
		while ($row = $result->fetch_assoc()) {
				
				array_push($category_list, $row['category_id']);
		}
	}

	
	mysqli_close($con);
	
	return $category_list;
}

function is_internation_package($package_id)
{
	include ("mysql_config.php");
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (mysqli_connect_errno()) {
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$query = "SELECT * FROM tour_category where package_id=$package_id and category_id=1";
	//echo $query;
	$result = mysqli_query($con, $query);
	mysqli_close($con);

	if( $result !== false && $row = mysqli_fetch_array($result)){
		//echo "international";
		return 1;
	}
	//echo "domestic";
	return 0;
}

function is_package_in_category($category,$pkg_category)
{
	//echo "category=".$category;
	//var_dump($pkg_category);
	foreach ($pkg_category as $val)
	{
		if($val==$category)
			return 1;
	}
	return 0;
}

function add_package_in_category($package, $category_list)
{
	include ("php-script/mysql_config.php");
	
	$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);

	if (!$con) {
		echo "Failed to connect to MySQL: " ;
		die('Error: ' );
	}
	
	//$query = "DELETE FROM tour_category WHERE package_id=".$package;
	//$result = mysqli_query($con, $query);
	
	foreach ($category_list as $val) {		
		
		// if($val==0)
		// 	continue;
		//var_dump($val);
		$query = 'INSERT INTO tour_category (package_id,
					category_id)
					VALUES ('.$package.','.$val.');';
		//echo "val= ".$val.",";
		if (!mysqli_query($con, $query)) {
			///die('Error: ' . mysqli_error($con));
			echo "categories added". "<br>"; 			
		}		
	}
	mysqli_close($con);
}

function delete_category($category_id)
{
	include ("php-script/mysql_config.php");
		$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
			if (mysqli_connect_errno()) {
				echo "Failed to connect to MySQL: " . mysqli_connect_error();
			}
	
	$query = "DELETE FROM tour_category WHERE category_id='".$category_id."'";	
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting catagory". "</br>"; 
		
	}
	
	$query = "DELETE FROM category_master WHERE id='".$category_id."'";
	
	
	if (!mysqli_query($con, $query)) {
		///die('Error: ' . mysqli_error($con));
		echo "error while deleting days". "</br>"; 
		
	}	
	mysqli_close($con);	
}
?>