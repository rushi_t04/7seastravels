<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Edit Package</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">	
		<link href="css/font-awesome.min.css" rel="stylesheet">    
	    <link href="css/main.css" rel="stylesheet">
	    <link href="css/package.css" rel="stylesheet">
	    <link href="css/responsive.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">
			html {
			  position: relative;
			  min-height: 100%;
			}
			body {
			  /* Margin bottom by footer height */
			  margin-bottom: 86px;
			}
			
		
			
		</style>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
		

	</head>

	<body>

		<!-- Navigation -->
		<?php
		include ("header.php");
		?>

		<!-- Page Content -->
		<div class="container">
			
			<?php
				require_once 'php-script/package.php';
				$requested_pkg = $_GET['package'];
				$pkg_details = get_package_details($requested_pkg);
				
				$days_list = get_package_days($requested_pkg);
				$category_list=get_category_list();
				$pkg_category=get_package_category($requested_pkg);	
				$images=get_package_images($requested_pkg);
				//var_dump($pkg_category);
				//foreach ($pkg_category as $var) {
					//	echo "\n".$var.",";
				//}
				/*echo "<br/>***************************";
				var_dump($days_list);
				echo "<br/>***************************";*/
			?>

			<form action="create-package-post.php" method="post" enctype="multipart/form-data">
				<div class="form-group">

					<div class="page-header">
						<h3>Modify Package</h3>
					</div>
					<input type="hidden" name="package_id" value=<?php echo $pkg_details['id']?> />
					<input type="hidden" name="package_image" value=<?php echo $pkg_details['image_name']?> />
					<div class="row">
						<div class="col-md-4">
							<div class="input-group">
								<span class="input-group-addon">Package Name</span>
								<input type="text" class="form-control" placeholder="Name"
								id="txt_package_name" name="txt_package_name" value="<?php echo $pkg_details['name']?>">
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon">Package Cost</span>
								<input type="text" class="form-control" placeholder="Cost"
								id="txt_package_cost" name="txt_package_cost" value="<?php echo $pkg_details['cost']?>">
							</div>
							<br>
							<div class="input-group">
								<span class="input-group-addon">Destinations</span>
								<input type="text" class="form-control" placeholder="Destinations" 
								id="txt_destinations" name="txt_destinations" value="<?php echo $pkg_details['destinations']?>">
							</div>
							</br>
							<div class="input-group">
								<span class="input-group-addon">Latitude Logitude</span>
								<input type="text" required="required" class="form-control" placeholder="Enter Lat long" value="<?php echo $pkg_details['lat_long']?>"
								id="txt_lat_long" name="txt_lat_long">
							</div>
						</div>
						<div class="col-md-8 ">
							<div class="input-group">
								<span class="input-group-addon">Package Description</span>
								<textarea class="form-control" rows="4" id="txt_package_description" 
								name="txt_package_description" ><?php echo $pkg_details['description']?></textarea>
							</div>
						</div>
					</div>
					<br>
					
					<div class="row" id="image_preview">	
						<div class="col-md-12">
							<div class="input-group">							
								<span class="input-group-addon">Select Image</span>
								<input name="files[]" id="filesToUpload" type="file" multiple="" />
								<!-- <input type="file" name="file" id="file" 
								accept="image/gif, image/jpeg, image/png"
								onchange="PreviewImage();">		 -->						
							</div>
						</div>
						<br>
						<?php
							foreach ($images as $image) {
								 echo '<div class="col-md-2"><div class="thumbnail">
							      <a class="close" href="#" id="'.$image.'">×</a>
							      <img src="images/package/'.$image.'">
							    </div></div>';
						    }
					    ?>
					</div>
					<br>
					<div class="row">
						<!-- <div class="col-md-3">
							<img class="img-responsive img-hover pull-right" id="uploadPreview"
							height="150" width="150"
							src="images/package/<?php echo $pkg_details['image_name']?>" alt="">
						</div>	 -->
						<div class="col-md-4">
							<div class="input-group">							
							<span class="input-group-addon">Select Category</span>
							<select multiple name="select_category[]">
								<option value="-1">none</option>
								<?php								
								foreach ($category_list as $category) {
									if(is_package_in_category($category['id'],$pkg_category)==1)
										echo '<option value="'.$category['id'].'" selected>'.$category['name'].'</option>';
									else
										echo '<option value="'.$category['id'].'" >'.$category['name'].'</option>';
								}	?>								
							</select> 
							</div>
						</div>
					</div>
					
					<div class="col-lg-12">
						<br />

						<div class="panel panel-default">

							<div class="panel-heading">
								Days
							</div>
							<div class="panel-body">
								<table id="dataTable" class="table" border="1">
									<tbody>
										<tr class="active">
											<td width="5%"></td>
											<td width="10%">Day</td>
											<td>Short Description</td>
											<td>Long Description</td>
										</tr>
										<?php
										foreach ($days_list as $val) {
											echo '<tr>
											<p>
											<td>
											<input type="checkbox" name="chk[]" checked="checked" />
											</td>
											<td>
											<input type="text"  placeholder=".col-lg-2" name="txt_day[]" required="required" class="form-control"
											value="' . $val['day'] . '"/>
											</td>
											<td>
											<input type="text"  placeholder=".col-lg-2" name="txt_short_desc[]" required="required" class="form-control"
											value="' . $val['short_desc'] . '"/>
											</td>
											<td>
											<input type="text"  placeholder=".col-lg-2" name="txt_long_desc[]" required="required" class="form-control"
											value="' . $val['long_desc'] . '"/>
											</td>
											</p>
											</tr>';
										}
										?>
									</tbody>
								</table>
							</div>
							<div pull-right>
								<input type="button" value="Add Day" onClick="addRow('dataTable')" />
								<input type="button" value="Remove Day" onClick="deleteRow('dataTable')"  />
							</div>
						</div>
					</div>
					<button type="submit" class="btn btn-primary">
					Submit
				</button>
					</div>
			</form>

			<div class="push"></div>

		</div>
		<!-- /.container -->

		 <!-- Footer -->
         <?php include ("footer.php");	?>
		 <script src="js/jquery.js"></script>
			<script src="js/bootstrap.min.js"></script>			
			<script src="js/main.js"></script>
			<script type="text/javascript">

			$('.close').click(function(){
				var id = $(this).attr('id');
				$(this).parent().parent().remove();
				var form_status = $('<div class="image_preview"></div>');
				$.ajax({
					url: 'php-script/delete-image.php',
					type: 'post',
					data: {
				        file: id,
				        package: <?php echo $requested_pkg?> 
				    },			
					beforeSend: function(){
						//form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
					}
				}).done(function(data){
					alert(data);
					// form_status.html('<p class="text-success">Thanks '+name+',<br> Your enquiry has been submitted successfully. Our travel advisor will get in touch with you shortly.</p>').delay(5000).fadeOut();
					// form[0].reset();
				});
			});

			function tableToJson(tableID) {
				var table = document.getElementById(tableID);
				var data = [];
				// first row needs to be headers
				var headers = [];
				for (var i = 0; i < table.rows[0].cells.length; i++) {
					headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
				}
				// go through cells
				for (var i = 1; i < table.rows.length; i++) {
					var tableRow = table.rows[i];
					var rowData = {};
					for (var j = 0; j < tableRow.cells.length; j++) {
						rowData[headers[1]] = tableRow.cells[1].innerHTML("txt_id");
						;
					}
					data.push(rowData);
				}

			}

			function submitData() {
				var jason = {
					"name" : "",
					"cost" : "",
					"Description" : "",
					"days" : [{
						"Day" : "John",
						"lastName" : "Doe"
					}, {
						"firstName" : "Anna",
						"lastName" : "Smith"
					}, {
						"firstName" : "Peter",
						"lastName" : "Jones"
					}]
				}
				alert(JSON.stringify(jason));
			}

			function addRow(tableID) {
				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				var row = table.insertRow(rowCount);
				var colCount = table.rows[0].cells.length;
				for (var i = 0; i < colCount; i++) {
					var newcell = row.insertCell(i);
					newcell.innerHTML = table.rows[1].cells[i].innerHTML;
				}

			}

			function deleteRow(tableID) {

				var table = document.getElementById(tableID);
				var rowCount = table.rows.length;
				for (var i = 1; i < rowCount; i++) {
					var row = table.rows[i];
					var chkbox = row.cells[0].childNodes[1];

					if (null != chkbox && true == chkbox.checked) {

						if (rowCount <= 2) {// limit the user from removing all the fields
							alert("Cannot Remove all the Passenger.");
							break;
						}
						table.deleteRow(i);
						rowCount--;
						i--;
					}
				}
			}
			function PreviewImage() {
		        var oFReader = new FileReader();
		        oFReader.readAsDataURL(document.getElementById("file").files[0]);
		
		        oFReader.onload = function (oFREvent) {
		            document.getElementById("uploadPreview").src = oFREvent.target.result;
        }
    };

		</script>
	</body>

</html>
