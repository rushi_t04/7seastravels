<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Add Master</title>

	<link href="css/bootstrap.min.css" rel="stylesheet">	
	<link href="css/font-awesome.min.css" rel="stylesheet">    
	<link href="css/main.css" rel="stylesheet">
	<link href="css/package.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>

		<!-- Navigation -->
		<?php 
		require_once 'header.php';
		?>

		<!-- Page Content -->
		<div class="container">
			
			<?php
			require_once 'php-script/package.php';
			
			if (!empty($_POST)) {
				include ("php-script/mysql_config.php");
				$con = mysqli_connect($dbhost, $dbuser, $dbpassword, $database);
				if (!$con) {
					echo "Failed to connect to MySQL: ";
					die('Error: ');
				}
				if (isset($_POST['txt_category_name'])) {

					$category_name = $_POST['txt_category_name'];

					

					$query = "INSERT INTO category_master (name)
					VALUES ('$category_name');";
					$result = mysqli_query($con, $query);
					if ($result) {
						echo '<div class="alert alert-success" role="alert">
						Category Sucessfullly Added.
					</div>';
				} else {
					echo '<div class="alert alert-danger" role="alert">
					Category Already Present</div>';
				}
			} 
			mysqli_close($con);	
		}

		?>
	</br>
	<form action="<?php $_PHP_SELF ?>" method="post" >
		<div class="row">
			<div class="col-lg-5">
				<div class="well">
					<div class="input-group">
						<span class="input-group-addon">Add Category</span>
						<input type="text" class="form-control" placeholder="Name" id="txt_category_name" name="txt_category_name"
						data-validation-required-message="Name is required" required="required">
					</input>
				</div></br>
				<button type="submit" class="btn btn-primary">
					Submit
				</button>
			</div>
		</div>
	</div>
</form>
			<!--<form action="<?php $_PHP_SELF ?>" method="post" >
				<div class="row">
					<div class="col-lg-5">
						<div class="well">
							<div class="input-group">
								<span class="input-group-addon">Add Location</span>
								<input type="text" class="form-control" placeholder="Name" id="txt_location_name" name="txt_location_name"
								data-validation-required-message="Name is required" required="required">
								</input>
							</div></br>
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
					</div>
				</div>
			</form>
			<form action="<?php $_PHP_SELF ?>" method="post" >
				<div class="row">
					<div class="col-lg-5">
						<div class="well">
							<div class="input-group">
								<span class="input-group-addon">Add Travel Mode</span>
								<input type="text" class="form-control" placeholder="Name" id="txt_travel_mode" name="txt_travel_mode"
								data-validation-required-message="Name is required" required="required">
								</input>
							</div></br>
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
					</div>
				</div>
			</form>

			<form action="<?php $_PHP_SELF ?>" method="post" >
				<div class="row">
					<div class="col-lg-5">
						<div class="well">
							<div class="input-group">
								<span class="input-group-addon">Add Hotel</span>
								<input type="text" class="form-control" placeholder="Name" id="txt_hotel" name="txt_hotel"
								data-validation-required-message="Name is required" required="required">
								</input>
							</div></br>
							<div class="input-group">
								<span class="input-group-addon">Hotel Location</span>
								<select id="hotel_location" class="form-control" name="hotel_location" required="required">
									<?php echo $location_option; ?>
								</select>
							</div></br>
							<button type="submit" class="btn btn-primary">
								Submit
							</button>
						</div>
					</div>
				</div>
			</form>-->
		</br>

		

	</div>
	<!-- /.container -->

	<!-- Footer -->
	<?php include ("footer.php");	?>
	 <script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>

</body>

</html>
