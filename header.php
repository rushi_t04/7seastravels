 
 <?php 
    session_start();
    require_once 'php-script/package.php';
   //var_dump($_POST);

   if (isset($_REQUEST['logout'])) {
        unset($_SESSION['is_admin']);
    }

   if(isset($_POST['username']) && isset($_POST['password'])){
        require_once 'php-script/auth.php';
        authenticate_user($_POST['username'],$_POST['password']);
   }
    //$_SESSION['is_admin']=1;

    function echoActiveClassIfRequestMatches($requestUri)
    {
        $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

        if ($current_file_name == $requestUri)
            echo '"scroll active"';
        else
            echo '"scroll"';
    }

    function isHome(){
         $current_file_name =basename($_SERVER['PHP_SELF']);

        if (strcmp($current_file_name, 'index.php') == 0){

            return true;
        }
        else
         return false;
    }

     $home='index.php';
    if(isHome()){
       $home = '';
       
    }

?>

 <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="images/blue-logo-1.png" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav" >
                        <li class="scroll active"><a href="<?php if(isHome()) echo '#home';else echo 'index.php'; ?>">Home</a></li>
                        <li class="scroll"><a href="<?php echo $home.'#portfolio'; ?>">Special Tours</a></li>
                       <li class="dropdown" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">International Tours<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <div class="row" style="width: 700px">
                                    <div class="col-md-4">
                                    <?php                                           
                                        $pkg_list = get_package_list(1);
                                        $cnt=0;
                                        foreach ($pkg_list as $pkg) {

                                            if( $cnt++%8==0 && $cnt!=1){
                                                echo '</div><div class="col-sm-4">';
                                            }
                                             echo ' <li>
                                                    <a href="show-package.php?package='.$pkg['id'].'">'.$pkg['name'].'</a>
                                                </li>';
                                        }
                                        
                                    ?>
                                    </div>
                                </div>
                            </ul>
                        </li>
                        
                        <li class="dropdown" >
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Domestic Tours<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <div class="row" style="width: 700px">
                                    <div class="col-md-4">
                                    <?php                                           
                                        $pkg_list = get_package_list(0);
                                        $cnt=0;
                                        foreach ($pkg_list as $pkg) {

                                            if( $cnt++%8==0 && $cnt!=1){
                                                echo '</div><div class="col-sm-4">';
                                            }
                                             echo ' <li>
                                                    <a href="show-package.php?package='.$pkg['id'].'">'.$pkg['name'].'</a>
                                                </li>';
                                        }
                                        
                                    ?>
                                    </div>
                                </div>
                            </ul>
                        </li>

                        <li class="scroll"><a href="<?php echo $home.'#services'; ?>">Services</a></li>
                        <li class="scroll"><a href="<?php echo $home.'#about'; ?>">About</a></li>
                        <li class="scroll"><a href="<?php echo $home.'#contact-us'; ?>">Contact</a></li>    
                         <?php if (isset($_SESSION['is_admin']) && $_SESSION['is_admin']==1): ?>
                           <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="create-package.php">Create New Package</a>
                                    </li>
                                    <li>
                                        <a href="edit-package-select.php">Modify Package</a>
                                    </li>
                                    <li>
                                        <a href="master-tables.php">Add Category</a>
                                    </li>
                                    <li>
                                        <a href="delete-package.php">Delete Package/Category</a>
                                    </li>
                                     <li>
                                        <a href='?logout'>Logout</a>
                                    </li>  
                                     <!--<li>
                                        <a href="manage-category.php">Manage Category</a>
                                    </li>-->
                                </ul>
                            </li>     
                        <?php else: ?>
                            <li>
                                <a href="" data-toggle="modal" data-target="#myModal" >Login</a>
                            </li>
                         <?php endif ?>                                    
                    </ul>                    
                        <!-- <form class="navbar-form navbar-left nav-search" role="search" action="search-package.php" method="post">
                           <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search" name="text_search"
                                    <?php if (!empty($_POST)) {             
                                    if (isset($_POST['text_search'])) {
                                        echo 'value="'.$_POST['text_search'].'"';
                                    }
                                }
                                ?>
                                >
                                <div class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                </div>
                            </div>
                        </form> -->
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
       <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">x</button>
            <h3>Login</h3>
          </div>
          <div class="modal-body">
            <form method="post" action="" name="login_form">
              <p><input type="text" class="form-control" id="username" name="username" placeholder="Username/Email"></p>
              <p><input type="password" class="form-control" id="password" name="password" placeholder="Password"></p>
              <p><button type="submit" class="btn btn-primary">Sign in</button>
               
              </p>
            </form>
          </div>
          <div class="modal-footer">
          
          </div>
    </div>
  </div>
</div>