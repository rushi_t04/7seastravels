	<!DOCTYPE html>
	<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Seven Seas Travels</title>
		<!-- core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/font-awesome.min.css" rel="stylesheet">
		<link href="css/animate.min.css" rel="stylesheet">
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.transitions.css" rel="stylesheet">
		<link href="css/prettyPhoto.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">    
		<link href="css/package.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->       
	    <link rel="shortcut icon" type="image/x-icon" href="images/ico/favicon.ico">
	    <style type="text/css">
	    	html {
			  position: relative;
			  min-height: 100%;
			}
			body {
			  /* Margin bottom by footer height */
			  margin-bottom: 86px;
			}
			div.form-group1 {
			position:absolute;
			z-index:0;
			top:20px; /* change to whatever you want */
			left:10px; /* change to whatever you want */
			right:auto; /* change to whatever you want */
			bottom:auto; /* change to whatever you want */
			}
	    </style>


	</head><!--/head-->

	<body id="home" class="homepage">

		<?php 
		require_once 'header.php';
		require_once 'php-script/package.php';		
		$category_list=get_category_list();
		?>

		<section id="main-slider">
			<header id="myCarousel" class="carousel slide">

				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
					<li data-target="#myCarousel" data-slide-to="5"></li>
					<li data-target="#myCarousel" data-slide-to="6"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner">
					<div class="item active">
						<div class="fill" style="background-image:url('images/slider/rajasthan.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
								<h2>Rajasthan</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/sikkim.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
								<h2>Sikkim</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/taj.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
								<h2>Agra</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/phuket2.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
								<h2>Phuket</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/australia-2.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
							<h2>Australia</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/maldives2.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
							<h2>Maldives</h2>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="fill" style="background-image:url('images/slider/suba-1.jpg');"></div>
						<div class="carousel-caption">
							<div class="carousel-Text">
							<h2>Mauritius</h2>
							</div>
						</div>
					</div>
					
				</div>
				<form action="search-package.php" method="post" >	  
				    <div class="form-group1 col-sm-4 col-sm-offset-4">
				      <div class="input-group input-group-lg right-block">
				        <input class="form-control" placeholder="Search Location" type="text" name="text_search">
				        <div class="input-group-btn" name="btn_search">
							<button class="btn btn-orange" type="submit"><i class="glyphicon glyphicon-search" style="margin-top: 2px;"></i></button>
						</div>
				      </div>
				    </div>
				</form>
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<span class="icon-prev"></span>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<span class="icon-next"></span>
				</a>
			</header>
		</section><!--/#main-slider-->


		<section id="portfolio">

			<div class="container">
				<div class="section-header">
					<h2 class="section-title text-center wow fadeInDown">Special tours</h2>
				</div>

				<div class="text-center">
					<ul class="portfolio-filter">
						<li><a class="active" href="#" data-filter="*">All</a></li>
						<?php

						$cnt=0;
						foreach ($category_list as $val) {
							//Skip Internation and domestic
							if( $val['id']==0 || $val['id']==1){
								 continue;
							}
							if($cnt++==0)
							{
								echo '<li>
								<a href="#" data-filter=".package_'.$cnt.'">'.
									$val['name']
									.'</a>
								</li>';
							}
							else
							{
								echo '<li>
								<a href="#" data-filter=".package_'.$cnt.'">'.
									$val['name']
									.'</a>
								</li>';
							}
						}
						?>	

					</ul><!--/#portfolio-filter-->
				</div>

				<div class="portfolio-items" >

					<?php
					$cnt=0;
					foreach ($category_list as $category) {
						//Skip Internation and domestic
						if( $category['id']==0 || $category['id']==1){
							 continue;
						}
						$cnt++;
						$pkg_list = get_package_list($category['id']);
						foreach ($pkg_list as $pkg) {
							echo '<div class="portfolio-item package_'.$cnt.'">
							<div class="package_con">
								<div class="package_img">
									<img class="img-responsive img-portfolio img-hover" src="images/package/' . $pkg['images'][0] .'" alt="">
								</div>

								<a href="show-package.php?package=' . $pkg['id'] .'" class="package_details">
									<h4>'.$pkg['name'].'</h4>
									<p>'. $pkg['days_count'] .' Days / '. ($pkg['days_count']-1) .' Nights</p><span class="book_now_wrap">
									<span>Read More<i class="sprite icon_book_arrow"></i></span>
								</span></a>
							</div>
						</div>';
					}
				}
				?>		

			</div>
		</div><!--/.container-->
	</section><!--/#portfolio-->


	<section id="services" >
		<div class="container">

			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown">Our Services</h2>
			</div>

			<div class="row">
				<div class="features">
					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-ticket"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Visa</h4>
								<p>We provide visa consultancy and are based in Pune. The company is very careful about the
									documentation which is handled by a dedicated team and every case is properly scrutinized before 
									submission. From the very beginning, the company advises on all important parameters and always 
									makes sure that clients are doing it properly.</p>
							</div>
						</div>
					</div><!--/.col-md-4-->

					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="100ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-newspaper-o"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Passport</h4>
								<p>We provide fully managed service for arranging Passport. We are aware of the process
									and fees structures and requirements and hence we provide flexible solutions.</p> 
								<p>Tatkal : For TATKAL passport cases, applicant have to meet the passport officer along with the file no 
								(Receipt) issued by the passport officer and the decision on the urgency is at the discretion of the 
								passport officer.</p>
							</div>
						</div>
					</div><!--/.col-md-4-->

					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="200ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-usd"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Forex</h4>
								<p>Foreign exchange can be issued only to Indian residents or foreigners who are resident permit
								holder. If you are travelling abroad we will help you for all your foreign exchange requirements.</p>
								<ul class="list-unstyled">
								   <li>Requirement of foreign exchange for Tourist purposes:
								    <ul>
								      <li>In one calendar year maximum of US $10,000 or its equivalent.</li>
								      <li>Per trip maximum in currency: US $3000 or its equivalent.</li>
								      <li>Other options are traveler's cheques or CWM Card.</li>
								    </ul>
								  </li>
								</ul>
							</div>
						</div>
					</div><!--/.col-md-4-->

					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="300ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-plus-square"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Travel Insurance</h4>
								<p>If you're traveling for business, studying abroad, or going on a trip with friends and
								family, you need travel insurance. We provide insurance for all types of travellers.</p>
							</div>
						</div>
					</div><!--/.col-md-4-->

					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="400ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-plane"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Air ticket</h4>
								<p> The company provides ticket booking services for International & Domestic Flights.</p>
							</div>
						</div>
					</div><!--/.col-md-4-->

					<div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="500ms">
						<div class="media service-box">
							<div class="pull-left">
								<i class="fa fa-h-square"></i>
							</div>
							<div class="media-body">
								<h4 class="media-heading">Hotel Booking</h4>
								<p>We provide Hotel Booking services to clients to their holiday destination. Depending upon the client’s budget, we can get luxurious or modest accommodations in reputed hotels in India</p>
							</div>
						</div>
					</div><!--/.col-md-4-->
				</div>
			</div><!--/.row-->    
		</div><!--/.container-->
	</section><!--/#services-->



	<section id="about">
		<div class="container">

			<div class="section-header">
				<h2 class="section-title text-center wow fadeInDown">About Us</h2>
			</div>

			<div class="row">
				<div class="col-sm-4 wow fadeInUp">
					<img class="img-responsive" src="images/common/travel-world-small-1.jpg" alt="">
				</div>

				<div class="col-sm-8 wow fadeInUp">
					<p><strong>Seaven Seas Travel</strong> is an efficiently running operational entity handled by smart individuals, 
						which will enable satisfaction not only those who receive services, but also those who offer 
						them. Primary motive is to produce the highest levels of client satisfaction in the industry 
						and Specialisation in customised tour packages both Domestic (within India) and 
						International (outside India)We are committed to offering travel services of the highest quality, combining our energy 
						and enthusiasm. Company have concentrated its efforts in producing quality travel, 
						responding to the needs of the times while anticipating the demands of the future. We have
						proudly served the travel needs of a diverse group of clients. We are real people with first 
						hand knowledge of creating a well-planned travel experience For you.</p>


						
						<div class="row ">
							<div class="col-sm-12 wow fadeInUp">
								<p>Seven Seas Travels is a part of <strong class="media-heading">Ceyone group</strong> which was formed in 2011 with the vision to establish upcoming trustworthy brand in
									various sectors like tourism, real estates & many more to come. We have a team of vast 
									experience people from various sectors and this team is constantly working hard towards
									our vision. We believe in constantly reinventing ourselves to overcome the challenges of a 
									dynamic business environment and ensure sustainable expansion.</p>      
								</div>  
							</div>	
						</div>
						<br>
					</div>
					<div class="row" style="padding-top: 20px">

						<img src = "images/team/neha.jpg" class = "img-circle center-block" style="height:150px;padding-bottom: 10px">
						<h3 class="media-heading text-center">Mrs. Neha Kulkarni</h3>
						<div  class="col-sm-12 wow fadeInUp">
							<p> As a part of this group, she lead the Tourism Business keeping the same 
								goals and vision of the group in the mind. Learning from her past 12 years experience at 
								Guardian Holidays Pune and 2 years of Thomas Cook experience as a Manager, She is now 
								all set for her own venture with new innovations and enthusiasm.
								Personally travelled and escorted tours all around the globe to name a few like Singaore-
								Thailand-Hongkong-Turkey-Switzerland-Egypt-Dubai-Europe-USA. She also has an expertise 
								of arranging and designing packages for destinations across the world. Having proficiency of 
								visas-passports-airtickets-forex, travel insurance makes it a one stop shop namely Seven 
								Seas Travels.</p>    
							</div>
						</div>
					</div>
				</section><!--/#about-->


				<section id="contact-us" class="contact">
						<div class="container" style="padding-bottom: 20px">
							<div class="section-header">
								<h2 class="section-title text-center wow fadeInDown">Contact Us</h2>
							</div>
							<div class="row">
								<div class="col-sm-4 wow fadeInLeft">
									<div class="contact-form">
										<h3>Contact Info</h3>

										<address>
											<strong>Sevan Seas Travels</strong><br>
											9, Rama Madhav Society,<br>
											Prabhat Road, Lane no. 9,<br>
											Erandwane,<br>
											Pune- 411 004,India<br>
											<strong>Phone: </strong> (+91) 9850 7070 17
										</address>

										<form id="main-contact-form" name="contact-form" action="php-script/mailer.php" method="post">
											<div class="form-group">
												<input type="text" id="name" name="name" class="form-control" placeholder="Name" required>
											</div>
											<div class="form-group">
												<input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
											</div>
											<div class="form-group">
												<input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number" required>
											</div>
											<div class="form-group">
												<textarea id="message" name="message" class="form-control" rows="8" placeholder="Message" required></textarea>
											</div>
											<button type="submit" class="btn btn-primary">Send Message</button>
										</form>
									</div>
								</div>
								<div class="col-sm-8 wow fadeInRight">
									<div id="google-map" style="height:585px" data-latitude="18.513523" data-longitude="73.835541"></div>
								</div>
							</div>

						</div>
			</section><!--/#bottom-->
			<br>
			<?php 
			include ("footer.php");		
			?>
			

			<script src="js/jquery.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
			<script src="js/owl.carousel.min.js"></script>
			<script src="js/mousescroll.js"></script>
			<script src="js/smoothscroll.js"></script>
			<script src="js/jquery.prettyPhoto.js"></script>
			<script src="js/jquery.isotope.min.js"></script>
			<script src="js/jquery.inview.min.js"></script>
			<script src="js/wow.min.js"></script>
			<script src="js/main.js"></script>
			<script type="text/javascript">
			  $(document).ready(function() {
			    $('.carousel').carousel({
			      interval: 4000
			    })
			  });
			</script>
		</body>
		</html>