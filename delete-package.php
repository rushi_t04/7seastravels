<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Delete Package</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">	
		<link href="css/font-awesome.min.css" rel="stylesheet">    
	    <link href="css/main.css" rel="stylesheet">
	    <link href="css/package.css" rel="stylesheet">
	    <link href="css/responsive.css" rel="stylesheet">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

	<body>

		<!-- Navigation -->
		<?php 
			require_once 'header.php';
		?>

		<br>

		<!-- Page Content -->
		<div class="container">
			
			<?php
				require_once 'php-script/package.php';	
				//Delete package/category			
				if (!empty($_POST)) {
					if (isset($_POST['select_package']))
					{
						//echo $_POST["select_package"];
						delete_package($_POST["select_package"]);
						echo '<div class="alert alert-success" role="alert">
								Package Deleted.
							</div>';
					}
					else if (isset($_POST['select_category']))
					{
						//echo $_POST["select_package"];
						delete_category($_POST["select_category"]);
						echo '<div class="alert alert-success" role="alert">
								Category Deleted.
							</div>';
					}
				}			

				//populate package/category
				$pkg_list = get_package_list();
				$category_list=get_category_list();
				$pkg_option = '<option value="0">' . "Select" . '</option>';
				foreach ($pkg_list as $val) {
					$pkg_option = $pkg_option . '<option value='.$val['id'].'>' . $val['name'] . '</option>';
				}
				
				$category_option = '<option value="0">' . "Select" . '</option>';
				foreach ($category_list as $val) {
					//Skip Internation and domestic
					if( $val['id']==0 || $val['id']==1){
						 continue;
					}
					$category_option = $category_option . '<option value='.$val['id'].'>' . $val['name'] . '</option>';
				}

			?>
			<form action="<?php $_PHP_SELF ?>" method="post" >
				<div class="row">
					<div class="col-lg-5">
						<div class="well">
							<div class="input-group">
								<span class="input-group-addon">Select Package</span>
								<select id="select_package" name="select_package" required="required"
								class="form-control" >
									<?php echo $pkg_option; ?>
								</select>
							</div><br>
							<button type="submit" class="btn btn-primary">
									Delete
							</button>
						</div>
					</div>	
				</div>	
			</form>
			
			<form action="<?php $_PHP_SELF ?>" method="post" >
				<div class="row">
					<div class="col-lg-5">
						<div class="well">
							<div class="input-group">
								<span class="input-group-addon">Select Category</span>
								<select id="select_category" name="select_category" required="required"
									class="form-control" >
									<?php echo $category_option; ?>
								</select>
							</div><br>
							<button type="submit" class="btn btn-primary">
									Delete
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.container -->

		 <!-- Footer -->
         <?php include ("footer.php");	?>
          <script src="js/jquery.js"></script>
			<script src="js/bootstrap.min.js"></script>

	</body>

</html>
